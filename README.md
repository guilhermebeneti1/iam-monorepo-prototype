# How to Run:

### First Step:

In the root file run:
pnpm install

## Second Step:

In the root file run:
pnpm --filter iam-models build

## Third Step:

In the root file run:
pnpm --filter iam-server dev

## Resource test:

http://localhost:3000
