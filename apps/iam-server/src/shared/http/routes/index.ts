import { Router } from "express";
import { User } from "iam-models";

const routes = Router();

const user: User = {
  firstName: "Guilherme",
  lastName: "Beneti",
  email: "guilherme@test.com"
}

routes.get('/', (req, res) => {
  res.json({
    status: 200,
    message: user
  });
})

export default routes;
